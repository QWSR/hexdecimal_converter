#include "stdio.h"

int main() {
	short c;
	char rem;

	while((c = getchar())!=EOF) {
		rem = (c & 0xF0) >> 4;

		if(rem > 0x9) {
			putchar(rem + 0x37);
		} else {
			putchar(rem + '0');
		}

		rem = c & 0xF;

		if(rem > 0x9) {
			putchar(rem + 0x37);
		} else {
			putchar(rem + '0');
		}

		putchar(' ');
	}

	return 0;
}
